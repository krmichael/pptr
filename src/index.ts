import puppeteer from "puppeteer";
import axios from "axios";

export interface UsersInterface {
  id: number;
  email: string;
  password: string;
  comment: string;
}

const LOGIN_URL =
  "https://accounts.google.com/signin/v2/identifier?service=youtube&uilel=3&passive=true&continue=https%3A%2F%2Fwww.youtube.com%2Fsignin%3Faction_handle_signin%3Dtrue%26app%3Ddesktop%26hl%3Den%26next%3Dhttps%253A%252F%252Fwww.youtube.com%252F&hl=en&ec=65620&flowName=GlifWebSignIn&flowEntry=ServiceLogin";

const TUBE_URL = "https://youtube.com";

const CHANNEL_ID = "UCssxz2U-3yt6GI9rCdw8U7Q";

// const VIDEO_ID = "vDGX7aLbCuo";

async function getUsers() {
  const response = await axios.get("http://localhost:3000/users");

  if (response.status === 200) return response.data;
}

async function getActiveUser() {
  const response = await axios.get("http://localhost:3000/activeUser");

  if (response.status === 200) return response.data;
}

async function updateActiveUser(userId: number) {
  await axios({
    method: "PUT",
    url: "http://localhost:3000/activeUser",
    data: {
      id: userId,
    },
    headers: {
      "Content-Type": "application/json",
    },
  });
}

async function getComment() {
  const response = await axios.get("http://localhost:3000/comments");

  if (response.status === 200) return response.data;
}

async function updateCommentId(commentId: number) {
  await axios({
    method: "PUT",
    url: "http://localhost:3000/comments",
    data: {
      id: commentId,
    },
    headers: {
      "Content-Type": "application/json",
    },
  });
}

async function getCommentById(commentId: number) {
  const response = await axios.get(
    `https://jsonplaceholder.typicode.com/comments/${commentId}`
  );

  if (response.status === 200) return response.data.name;
}

const main = async () => {
  const browser = await puppeteer.launch({
    product: "firefox",
    // headless: false,
    // defaultViewport: null,
    // args: ["--start-maximized"],
  });

  const page = await browser.newPage();

  const activeUser = await getActiveUser();
  const users = await getUsers();

  await botLoad(page, browser, users[activeUser.id]);
};

main();

async function botLoad(
  page: puppeteer.Page,
  browser: puppeteer.Browser,
  user: UsersInterface
) {
  // Navigate
  await page.goto(LOGIN_URL, { waitUntil: "load" });

  // Login
  await Login(page, user);

  // Set Language === Portuguese
  await updateLanguage(page);

  // wait 5 seconds
  await page.waitForTimeout(5000);

  // Naviagate to channel
  await goToChannel(page);

  // Get First Video and Click
  await getAndClickOnFirstVideo(page);

  // OR

  // Navigate from Video and Open
  // await navigateToVideo(page, VIDEO_ID);

  // wait 3 seconds
  await page.waitForTimeout(1000);

  // Reset Video
  // await reset(page);

  // Mute video
  await mute(page);

  // Play on Video
  await play(page);

  // Autonav Disable
  await autonav(page);

  // Watching...
  await page.waitForTimeout(60000); // Wait 60 seconds

  // Like
  await addLike(page);

  // wait 3 seconds
  await page.waitForTimeout(1000);

  // Notifications
  await activeNotification(page);

  // wait 3 seconds
  await page.waitForTimeout(1000);

  // Subscribe
  await subscribe(page);

  // wait 3 seconds
  await page.waitForTimeout(1000);

  // Add comment
  await addComment(page);

  // wait 3 seconds
  await page.waitForTimeout(1000);

  // Logout of account
  await Logout(page);

  // wait 3 seconds
  await page.waitForTimeout(1000);

  // Close browser
  await closeBrowser(browser);

  // All Users
  const users = await getUsers();

  // Reset active user id for next video
  if (user.id === users.length - 1) {
    await updateActiveUser(0);
    return;
  }

  // Update Active User Id
  await updateActiveUser((user.id += 1));

  // Repeat
  await main();

  // Repeat Loop
  // if (user.id === users.length - 1) {
  //   await updateActiveUser(0); // End
  // } else {
  //   await updateActiveUser((user.id += 1));
  // }

  // await main();
}

/**
 * Logout the youtube account
 * @param page puppeteer.Page
 */
async function Logout(page: puppeteer.Page) {
  // Open modal
  await (await page.waitForSelector("button#avatar-btn"))?.click();

  // wait 1 seconds
  await page.waitForTimeout(1000);

  // Click for Logout
  await (await page.waitForSelector('a[href="/logout"]'))?.click();
}

/**
 * Close browser and exit bot
 * @param browser puppeteer.Browser
 */
async function closeBrowser(browser: puppeteer.Browser) {
  await browser.close();
}

/**
 * Next step from login popup
 * @param page puppeteer.Page
 * @param identifier string
 */
async function Next(page: puppeteer.Page, identifier: string) {
  await (await page.$(identifier))?.click();
}

/**
 * Login
 * @param page puppeteer.Page
 */
async function Login(page: puppeteer.Page, user: UsersInterface) {
  // Set email value
  await (
    await page.waitForSelector("[aria-label='Email or phone']")
  )?.type(user.email, { delay: 250 });

  // Next
  await Next(page, "#identifierNext");

  // wait 3 seconds
  await page.waitForTimeout(3000);

  // Set Pass value
  await (
    await page.waitForSelector("#password")
  )?.type(user.password, { delay: 250 });

  // Next
  await Next(page, "#passwordNext");
}

/**
 * Set Youtube Language for PT-BR
 * @param page puppeteer.Page
 */
async function updateLanguage(page: puppeteer.Page) {
  // Open modal
  await (await page.waitForSelector("button#avatar-btn"))?.click();

  // wait 1 seconds
  await page.waitForTimeout(1000);

  // Click on Language
  await page.evaluate(() => {
    const menuItems = [
      ...document.querySelectorAll(
        "a.yt-simple-endpoint.style-scope.ytd-compact-link-renderer"
      ),
    ];

    // @ts-ignore
    if (menuItems && menuItems.length > 5) menuItems[5].click();
  });

  // wait 1 seconds
  await page.waitForTimeout(1000);

  await page.evaluate(() => {
    const langItems = [
      ...document.querySelectorAll(
        "a.yt-simple-endpoint.style-scope.ytd-compact-link-renderer"
      ),
    ];

    console.log("langItems: ", langItems);

    // @ts-ignore
    if (langItems && langItems.length > 47) langItems[47].click();
  });
}

/**
 * Navigate to channel
 * @param page puppeteer.Page
 */
async function goToChannel(page: puppeteer.Page) {
  await page.goto(TUBE_URL + "/channel/" + CHANNEL_ID);
}

/**
 * Get and Click at First Video the channel
 * @param page puppeteer.Page
 */
async function getAndClickOnFirstVideo(page: puppeteer.Page) {
  await page
    .waitForSelector("#thumbnail")
    .then((first) => first?.click())
    .catch((error) => console.log("first video not found", error));
}

/**
 * Navigate to video
 * @param page puppeteer.Page
 */
async function navigateToVideo(page: puppeteer.Page, videoId: string) {
  await page.goto(TUBE_URL + "/watch?v=" + videoId);
}

/**
 * Play on video
 * @param page puppeteer.Page
 */
async function play(page: puppeteer.Page) {
  await page.evaluate(() => {
    const isPaused: HTMLButtonElement | null = document.querySelector(
      "[aria-label='Reproduzir (k)']"
    );

    if (isPaused) isPaused.click(); // Play
  });
}

/**
 * Autonav Disable on video
 * @param page puppeteer.Page
 */
async function autonav(page: puppeteer.Page) {
  await page
    .waitForSelector("[data-tooltip-target-id='ytp-autonav-toggle-button']")
    .then((selector) => selector?.click())
    .catch((error) => console.log("Autonav button not found ", error));
}

/**
 * Reset Video
 * @param page puppeteer.Page
 */
async function reset(page: puppeteer.Page) {
  await page.evaluate(() => {
    const video: HTMLVideoElement | null = document.querySelector(
      "video.video-stream.html5-main-video"
    );

    console.log("video: ", video);

    if (video) {
      video.load();
      video.currentTime = 0;
    }
  });
}

/**
 * Mute video
 * @param page puppeteer.Page
 */
async function mute(page: puppeteer.Page) {
  await page.evaluate(() => {
    const isMuted: HTMLButtonElement | null = document.querySelector(
      "button.ytp-mute-button.ytp-button"
    );

    if (isMuted) isMuted.click(); // Mute
  });
}

/**
 * Add like on video
 * @param page puppeteer.Page
 */
async function addLike(page: puppeteer.Page) {
  const liked = await page.$(
    ".style-scope.ytd-toggle-button-renderer.style-default-active"
  );

  if (liked) return;

  await page.evaluate(() => {
    const btnLike: HTMLButtonElement | null = document.querySelector(
      "[aria-label^='Marque este vídeo como']"
    );

    if (btnLike) btnLike.click();
  });
}

/**
 * Active Notifications
 * @param page puppeteer.Page
 */
async function activeNotification(page: puppeteer.Page) {
  const allowNotifications = await page.$(
    "[aria-label^='Com a configuração atual, você recebe todas as notificações']"
  );

  if (allowNotifications) return;

  await page.evaluate(() => {
    const notificationIcon: HTMLButtonElement | null = document.querySelector(
      ".ytd-subscription-notification-toggle-button-renderer"
    );

    if (notificationIcon) notificationIcon.click();
  });

  // Wait 1 second
  await page.waitForTimeout(1000);

  await page.evaluate(() => {
    const allNotifications: HTMLButtonElement | null = document.querySelector(
      ".ytd-menu-popup-renderer.iron-selected"
    );

    if (allNotifications) allNotifications.click();
  });
}

/**
 * Subscribe on the channel
 * @param page puppeteer.Page
 */
async function subscribe(page: puppeteer.Page) {
  await page.evaluate(() => {
    const btnSubscribe: HTMLButtonElement | null = document.querySelector(
      "[aria-label^='Inscreva-se em ']" // CHANGE IT
    );

    if (btnSubscribe) btnSubscribe.click();
  });
}

/**
 * Add comment video
 * @param page puppeteer.Page
 */
async function addComment(page: puppeteer.Page) {
  await page.evaluate(() => {
    // window.scrollBy(0, 500);
    window.scrollBy(0, window.innerHeight);
  });

  // Wait 3 seconds
  await page.waitForTimeout(3000);

  const comment = await getComment();
  const currentComment: string = await getCommentById(comment.id);

  await page
    .waitForSelector("#simplebox-placeholder")
    .then(async (input) => {
      await input?.click();
      await input?.type(currentComment, { delay: 250 });
    })
    .catch((error) => console.log("comment field not found", error));

  // Wait 1 second
  await page.waitForTimeout(1000);

  // Click on comment button
  await (await page.$("[aria-label='Comentar']"))?.click();

  // Update commentId
  await updateCommentId((comment.id += 1));
}
